package ms.ktac.configserver;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.tuckey.web.filters.urlrewrite.Conf;
import org.springframework.core.io.Resource;
import org.tuckey.web.filters.urlrewrite.UrlRewriteFilter;
import javax.servlet.*;
import java.io.IOException;

@Component
public class ConfigUrlRewriteFilter extends UrlRewriteFilter{
    private static final String CONFIG_LOCATION = "classpath:/urlrewrite.xml";

    @Value(CONFIG_LOCATION)
    private Resource urlrewrite;

    @Override
    protected void loadUrlRewriter(FilterConfig filterConfig) throws ServletException {
        try {
            Conf conf = new Conf(filterConfig.getServletContext(), urlrewrite.getInputStream(), urlrewrite.getFilename(), "");
            checkConf(conf);
        } catch (IOException ex) {
            throw new ServletException("Unable to load URL rewrite configuration file from " + CONFIG_LOCATION, ex);
        }
    }
}