package ms.ktac.configserver;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.config.server.EnableConfigServer;
import org.springframework.context.annotation.Bean;
import org.tuckey.web.filters.urlrewrite.UrlRewriteFilter;


@Slf4j
@SpringBootApplication
@EnableConfigServer
@EnableDiscoveryClient
public class ConfigServerApplication extends SpringBootServletInitializer {

	@Bean
	public UrlRewriteFilter getUrlRewriteFilter() {
		//log.debug("Calling Bean URL Rewrite Filter");
		return new UrlRewriteFilter();
	}

	public static void main(String[] args) {
		SpringApplication.run(ConfigServerApplication.class, args);
	}

}
